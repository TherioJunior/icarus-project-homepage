<section id="about">
    <h2 id="introduction-header">I'm TherioJunior</h2>
    <br>

    <h3 id="skills-info">Languages and frameworks I can code</h3>
    <ul class="left-column">
        <li><span class="checkmark">&#10004;</span> LUA</li>
        <li><span class="checkmark">&#10004;</span> Python</li>
        <li><span class="checkmark">&#10004;</span> JavaScript</li>
        <li><span class="checkmark">&#10004;</span> PHP</li>
        <li><span class="checkmark">&#10004;</span> HTML</li>
        <li><span class="checkmark">&#10004;</span> CSS</li>
    </ul>
    <ul class="right-column">
        <li><span class="checkmark">&#10004;</span> C#</li>
        <li><span class="checkmark">&#10004;</span> C++</li>
        <li><span class="checkmark">&#10004;</span> Java</li>
    </ul>

    <h3 id="experience-info">Fields I have experience in</h3>
    <div id="experience-list-div">
        <ul class="experience">
            <li><span class="checkmark">&#10004;</span> Windows Kernel Development</li>
            <li><span class="checkmark">&#10004;</span> Windows Desktop Applications</li>
            <li><span class="checkmark">&#10004;</span> Reverse Engineering</li>
            <li><span class="checkmark">&#10004;</span> Scripting</li>
            <li><span class="checkmark">&#10004;</span> Web Development</li>
        </ul>
    </div>
</section>