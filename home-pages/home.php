<section id="home">
    <h2 id="introduction-header">Welcome to ICARUS</h2>
    <br>

    <p>I'm TherioJunior, privacy focused to a psychotic degree and aim to provide means to protect your privacy online :)</p>
    <p>If you want to find out more about me or rather my skills, check out the <a href="?page=about">About section</a>.</p>
    <br>
    <p>This server provides some information about me, links my GitLab, and has some other nice things hosted such as two privacy focused meta search engines.</p>
    <p>The first being my favourite <a href="https://docs.searxng.org/" id="middle-sentence-hrefs">SearXNG</a> which can be found <a href="/searxng">here</a>, the second of which being <a href="https://github.com/hnhx/librex">LibreX</a>, which can be found <a href="/librex">here</a>.</p>
    <br>
    <p>For my GitLab and some invite only projects of mine, please refer to the <a href="?page=projects">Projects section</a>.</p>
    <p>For more pages hosted on this server, please refer to the <a href="?page=pages">Pages section</a>.</p>
    <p>If you wish to contact me somehow, information is linked on my GitLab or also in the <a href="?page=contact">Contact section</a>.</p>
</section>