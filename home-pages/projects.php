<section id="projects">
    <h2 id="introduction-header">Projects</h2>
    <br>
    
    <p>If you want to go straight to my GitLab and check out my profile and projects there, please click <a href="https://gitlab.com/TherioJunior">here</a>.</p>

    <br>

    <p>If you just want to read about projects I currently develop or have developed in the past, you can continue reading :)</p>
    <p>Some of those projects are:</p>
    <p><a href="https://gitlab.com/TherioJunior/icarus-project-homepage">This page</a></p>
    <p><a href="https://gitlab.com/TherioJunior/jarvis-moe-ai-model">MoE AI Model</a></p>
    <p><a href="https://gitlab.com/TherioJunior/jarvis-voice-assistant">Whisper powered PC Voice Assistant</a></p>
    <p><a href="https://gitlab.com/TherioJunior/chrome-extension-dumper">Chrome Extension Dumper</a></p>
    <p><a href="https://gitlab.com/TherioJunior/linux">Linux repository</a></p>
    <p><a href="https://gitlab.com/TherioJunior/dynamic-analysis-writeups">Dynamic Analysis of popular applications</a></p>
    <p>Two ChatGPT extensions for Firefox which technically speaking aren't needed or all that useful anymore, and some <a href="https://gitlab.com/TherioJunior/projects">other projects</a>.</p>

    <br>

    <p>Regarding the invite only projects I have talked about in the <a href="?page=home">home section</a>, they are cheats for video games such as:</p>
    <p>CS:GO, CS:GO 2018 & CS 2</p>
    <p>Minecraft</p>
    <p>Sea Of Thieves</p>
    <p>CoD: BO 3 & Battlefield 4</p>
    <p>Genshin Impact</p>
    <p>And some other games I shouldn't mention.</p>
    <p>The scope these are done in range from externals, to internals, to kernel as well as KVM (Kernel Virtual Machine) based.</p>
</section>