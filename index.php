<!DOCTYPE html>
<html lang="en">
    <head>
        <title>ICARUS Project</title>
        <meta charset="UTF-8">
        <!--- <meta name="viewport" content="width=device-width, initial-scale=1.0"> --->
        <!--- Load global stylesheet controlling all the navigation bar aspects --->
        <link rel="stylesheet" href="stylesheets/style-global.css">
        <!--- Load specific section stylesheet --->
        <?php
        $pages = array("home", "projects", "pages", "contact", "about");
        $default = "home"; // Default page
        $page = $_GET["page"] ?? $default;

        if (in_array($page, $pages)) {
            ?><link rel="stylesheet" href="stylesheets/style-<?php echo $page; ?>-section.css"><?php
        }
        ?>
    </head>

    <body>
        <!--- Top navigation bar --->
        <header>
            <div id="logo-picture">
                <a href="?page=home">
                    <img src="assets/logo.png" alt="Logo" width="35" height="35">
                </a>
            </div>
            <div id="brand"></div>
            <span id="brand-author"> by TherioJunior</span>
            <nav>
                <ul class="nav-list">
                    <li><a href="?page=home">Home</a></li>
                    <li><a href="?page=projects">Projects</a></li>
                    <li><a href="?page=pages">Pages</a></li>
                    <li><a href="?page=contact">Contact</a></li>
                    <li><a href="?page=about">About</a></li>
                </ul>
            </nav>
        </header>

        <main>
            <section id="content">
                <?php
                $pages = array("home", "projects", "pages", "contact", "about");
                $default = "home"; // Default page
                $page = $_GET["page"] ?? $default;

                if (in_array($page, $pages)) {
                    include("home-pages/" . $page . ".php");
                } else {
                    include("home-pages/" . $default . ".php");
                }
                ?>
            </section>
        </main>

        <script src="script.js"></script>
    </body>
</html>