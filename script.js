document.addEventListener('DOMContentLoaded', function () {
    const brandText = document.getElementById('brand');
    let opacity = 1;
    let fadingOut = true;

    function pulsate() {
        if (fadingOut) {
            opacity -= 0.01; // Slow down the animation

            if (opacity <= 0.5) { // Keep the text visible while pulsating
                fadingOut = false;
            }
        } else {
            opacity += 0.01; // Slow down the animation

            if (opacity >= 1) {
                fadingOut = true;
            }
        }

        brandText.style.opacity = opacity.toString();
        requestAnimationFrame(pulsate);
    }

    pulsate();
    typeWriter();
});

// Credits: https://www.w3schools.com/howto/howto_js_typewriter.asp
// Credits: TherioJunior for slightly adjusting this logic so it also deletes the text.
var i = 0;
var txt = 'ICARUS';
var speed = 150;
var deleting = false; // A flag to track deleting

function typeWriter() {
    var brandDiv = document.getElementById("brand");

    if (!deleting) {
        if (i < txt.length) {
            brandDiv.innerHTML += txt.charAt(i);
            i++;
            setTimeout(typeWriter, speed);
        } else {
            // Once we've displayed the full text, start deleting after a pause
            setTimeout(function () {
                deleting = true;
                typeWriter();
            }, 2000);
        }
    } else {
        if (i > 0) {
            // Remove characters one by one
            brandDiv.innerHTML = txt.substring(0, i - 1) + " ";
            i--;
            setTimeout(typeWriter, speed);
        } else {
            // Once we've deleted the full text, start typing again after a pause
            deleting = false;
            setTimeout(typeWriter, 1000);
        }
    }
}
